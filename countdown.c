///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example Usage:
//   ./countdown
//
// Results:
//   Reference Time:  Tue Jan 21 04:26:07 PM HST 2014
//   Years: 8 Days: 33 Hours: 12 Minutes: 19 Seconds: 30
//   Years: 8 Days: 33 Hours: 12 Minutes: 19 Seconds: 31
//   Years: 8 Days: 33 Hours: 12 Minutes: 19 Seconds: 32
//   Years: 8 Days: 33 Hours: 12 Minutes: 19 Seconds: 33
//   Years: 8 Days: 33 Hours: 12 Minutes: 19 Seconds: 34
//
//
// @author Leo Liang <leoliang@hawaii.edu>
// @date   21_2_2022
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<string.h>

#define seconds_in_a_year 31536000
#define seconds_in_a_day 86400
#define seconds_in_a_hour 3600
#define seconds_in_a_minute 60

int main() {
   struct tm start={.tm_mday=21, .tm_min=26, .tm_sec=7, .tm_year=114, .tm_wday=1, .tm_yday=51, .tm_hour=4,};
   time_t t = mktime(&start);
   printf("Reference time:  ");
   printf("Tue Jan 21 04:26:07");
   printf("PM HST 2014 \n");
   int reference_seconds= t;

   while(1){
      int current=time(NULL);
      int time_diff= current - reference_seconds;
      int years= time_diff / seconds_in_a_year;
      printf("Years: %d ", years);

      int year_seconds = years * seconds_in_a_year;
      int new_seconds = time_diff - year_seconds;
      int days= new_seconds / seconds_in_a_day;
      printf("Days: %d ", days);

      int day_seconds= days * seconds_in_a_day;
      int day_new_seconds = new_seconds - day_seconds;
      int hours = day_new_seconds / seconds_in_a_hour;
      printf("Hours: %d ", hours);

      int hour_seconds = hours * seconds_in_a_hour;
      int hour_new_seconds = day_new_seconds - hour_seconds;
      int minutes = hour_new_seconds / seconds_in_a_minute;
      printf("Minutes: %d ", minutes);

      int min_seconds = minutes * seconds_in_a_minute;
      int min_new_seconds = hour_new_seconds - min_seconds;
      printf("Seconds: %d \n", min_new_seconds);

      sleep(1);

   }
   return 0;
}
